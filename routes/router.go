package routes

import (
	"log"

	"bitbucket.com/fantom-cache/endpoints"
	"github.com/gorilla/mux"
)

//Router é uma função usada para estabelecer as rotas da API de cache
func Router() (r *mux.Router) {
	r = mux.NewRouter()

	log.Println("Mapeando endpoints...")
	log.Println("Endpoint [DELETE] /caches/{storekey} disponível...")
	log.Println("Endpoint [POST] /caches/{storekey}/{itemid} disponível...")
	log.Println("Endpoint [POST] /caches/{storekey}/batch disponível...")
	log.Println("Endpoint [GET] /caches/{storekey}/{itemid} disponível...")
	log.Println("Endpoint [GET] /caches/{storeKey} disponivel...")
	log.Println("Endpoint [GET] /caches disponível...")

	r.HandleFunc("/caches/{storekey}/batch", endpoints.BatchInsert).Methods("POST")
	r.HandleFunc("/caches/{storekey}/{itemid}", endpoints.UpsertCache).Methods("POST")
	r.HandleFunc("/caches/{storekey}/{itemid}", endpoints.GetCacheByStoryKeyAndID).Methods("GET")
	r.HandleFunc("/caches/{storekey}", endpoints.DeleteCacheSector).Methods("DELETE")
	r.HandleFunc("/caches/{storekey}", endpoints.GetCacheByStoreKey).Methods("GET")
	r.HandleFunc("/caches", endpoints.GetCaches).Methods("GET")

	return
}
