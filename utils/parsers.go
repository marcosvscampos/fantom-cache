package utils

import (
	"io"
	"io/ioutil"
)

//ParseBodyToStr converte um Body do request e retorna um formato String
func ParseBodyToStr(body io.ReadCloser) (parsedBody string, err error) {
	bodyContent, err := ioutil.ReadAll(body)
	if err != nil {
		return
	}
	parsedBody = string(bodyContent)
	return
}
