package main

import (
	"log"
	"net/http"

	"bitbucket.com/fantom-cache/routes"
	"github.com/gorilla/handlers"
)

var (
	port = ":4000"
)

func main() {
	router := routes.Router()

	allowedHeaders := handlers.AllowedHeaders([]string{"X-Requested-With, Content-Type"})
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	log.Println("Backend iniciado na porta 4000")
	if err := http.ListenAndServe(port, handlers.CORS(allowedHeaders, allowedMethods, allowedOrigins)(router)); err != nil {
		log.Printf("Erro ao iniciar backend na porta %s - %s \r\n", port, err.Error())
		return
	}
}
