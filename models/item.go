package models

//Item representa um item que será salvo no cache local
type Item struct {
	Key     string `json:"key"`
	Content []byte `json:"content"`
}
