package endpoints

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.com/fantom-cache/models"
	"bitbucket.com/fantom-cache/utils"
	"github.com/gorilla/mux"
)

var (
	list  []models.Item
	store = make(map[string][]models.Item)
)

func retrieveOriginalValueQueryParam(r *http.Request) (originalValue bool) {
	originalValue = false
	queryParam := r.URL.Query().Get("originalValues")
	if queryParam != "" {
		originalValue, _ = strconv.ParseBool(queryParam)
	}
	return
}

func parseByteListToOriginalMap(list *[]models.Item) (originalList []map[string]interface{}, err error) {
	for _, v := range *list {
		objMap := make(map[string]interface{})
		objMap["id"] = v.Key
		err = json.Unmarshal(v.Content, &objMap)
		originalList = append(originalList, objMap)
	}
	return
}

func parseByteToOriginalMap(item *models.Item) (originalMap map[string]interface{}, err error) {
	originalMap = make(map[string]interface{})
	originalMap["id"] = item.Key
	err = json.Unmarshal(item.Content, &originalMap)
	return
}

//GetCacheByStoryKeyAndID recupera um registro único dado o a coleção do cache e sua chave
func GetCacheByStoryKeyAndID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	w.Header().Set("content-type", "application/json")
	storekey := params["storekey"]
	itemid := params["itemid"]

	list := store[storekey]
	if list == nil {
		list = make([]models.Item, 0)
	}

	for _, v := range list {
		if v.Key == itemid {
			fmt.Printf("Caches item in store - %+v \n", v.Content)
			if ov := retrieveOriginalValueQueryParam(r); ov == true {
				originalMap, err := parseByteToOriginalMap(&v)
				if err != nil {
					log.Println("Erro ao recuperar valores originais", err.Error())
					w.WriteHeader(http.StatusInternalServerError)
					w.Write([]byte("500 - Internal Server Error"))
				}
				fmt.Printf("Original Values in cache %s - %+v", storekey, originalMap)
				json.NewEncoder(w).Encode(originalMap)
				return
			}
			fmt.Printf("Caches in store %s - %+v \n", storekey, v)
			json.NewEncoder(w).Encode(v)
		}
	}
	w.WriteHeader(http.StatusNotFound)
}

//GetCacheByStoreKey recupera um conjunto de caches conforme o store passado
func GetCacheByStoreKey(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	w.Header().Set("content-type", "application/json")
	storekey := params["storekey"]

	list := store[storekey]
	if list == nil {
		list = make([]models.Item, 0)
	}

	if ov := retrieveOriginalValueQueryParam(r); ov == true {
		originalList, err := parseByteListToOriginalMap(&list)
		if err != nil {
			log.Println("Erro ao recuperar valores originais", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("500 - Internal Server Error"))
		}
		fmt.Printf("Original Values in cache %s - %+v", storekey, originalList)
		json.NewEncoder(w).Encode(originalList)
		return
	}

	fmt.Printf("Caches in store %s - %+v \n", storekey, list)
	json.NewEncoder(w).Encode(list)

}

//GetCaches retorna todos os registros disponiveis no cache
func GetCaches(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")

	fmt.Printf("Caches %+v \n", store)
	json.NewEncoder(w).Encode(store)
}

//BatchInsert adiciona valores a partir de uma lista
func BatchInsert(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	storekey := params["storekey"]
	list := store[storekey]
	item := models.Item{}
	var objArray []interface{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&objArray)
	if err != nil {
		log.Println("Erro ao parsear request", err.Error())
		return
	}

	for _, v := range objArray {
		fmt.Printf("Inserting value '%v' in store '%s' \n", v, storekey)
		jsonStr, err := json.Marshal(v)
		if err != nil {
			log.Println("Erro ao parsear request", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		id, err := utils.GenerateRandomID()
		if err != nil {
			log.Println("Erro ao gerar ID para registro", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		item.Key = id
		item.Content = []byte(jsonStr)

		list = append(list, item)
	}
	store[storekey] = list
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(fmt.Sprintf("Novos valores adicionados com sucesso!")))
}

//UpsertCache adiciona/atualiza um registro no cache
func UpsertCache(w http.ResponseWriter, r *http.Request) {
	item := models.Item{}
	params := mux.Vars(r)
	w.Header().Set("content-type", "application/json")

	body, err := utils.ParseBodyToStr(r.Body)
	if err != nil {
		log.Println("Erro ao parsear request", err.Error())
		return
	}

	item.Key = params["itemid"]
	item.Content = []byte(body)

	storekey := params["storekey"]
	list := store[storekey]

	for i := range list {
		if list[i].Key == item.Key {
			list[i].Content = item.Content
			store[storekey] = list
			log.Printf("Novo item %s atualizado com sucesso! \n", item.Key)
			w.WriteHeader(http.StatusCreated)
			w.Write([]byte(fmt.Sprintf("Novo item %s atualizado com sucesso! \n", item.Key)))
			return
		}
	}

	list = append(list, item)
	store[storekey] = list
	log.Println("Novo item inserido com sucesso!")
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Novo item inserido com sucesso!"))
}

//DeleteCacheSector remove uma coleção de cache inteira
func DeleteCacheSector(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	storekey := params["storekey"]
	delete(store, storekey)

	res := fmt.Sprintf("Coleção %s removido com sucesso!", storekey)
	log.Println(res)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(res))
}
